﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;

namespace AutomationFrameWork
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class AutomateClientSettings
    {
        public AutomateClientSettings()
        {
        }

        [TestInitialize]
        public void InitiateBrowser()
        {
            BrowserWindow.Launch("http://development.agileitsolutions.net");
        }
        [TestMethod]
        public void flightdeck()
        {
            //Login to the FlightDeck Application
            login();
            //Add the Profile
            AddProfile();
           
        }
        public void login()
        {
            AutomationService.EnterText<HtmlEdit>(AutomationService.PropertyType.Name, "UserName", "dipesh");
            AutomationService.EnterText<HtmlEdit>(AutomationService.PropertyType.Id, "txtPassword", "dipesh");
            AutomationService.Click<HtmlInputButton>(AutomationService.PropertyType.Name, "login");

        }
        public void AddProfile()
        {
            AutomationService.mParentWindow = null;
            AutomationService.WaitForControl<HtmlHyperlink>(AutomationService.PropertyType.Id, "IdMyProjects");
            AutomationService.Click<HtmlHyperlink>(AutomationService.PropertyType.Id, "IdMyProjects");
            AutomationService.Click<HtmlHyperlink>(AutomationService.PropertyType.InnerText, "Add Profile");
            AutomationService.EnterText<HtmlEdit>(AutomationService.PropertyType.Id, "profilename", "APofile1");
            AutomationService.EnterText<HtmlEdit>(AutomationService.PropertyType.Id, "name", "AClient1");
            AutomationService.EnterText<HtmlEdit>(AutomationService.PropertyType.Id, "Website", "Atest1.com.au");
            AutomationService.EnterText<HtmlEdit>(AutomationService.PropertyType.Id, "address", "AAddress1");           
            AutomationService.SelectDDLItem<HtmlComboBox>(AutomationService.PropertyType.Name, "ddlFrequency", "Monthly");
            //AutomationService.SelectDDLItem<HtmlComboBox>(AutomationService.PropertyType.Name, "ddlSiteTypeList", "Informational");
            AutomationService.SelectCheckbox<HtmlCheckBox>(AutomationService.PropertyType.Id, "trackMobileRanking", "check");




        }


     
        #region Additional test attributes

            // You can use the following additional attributes as you write your tests:

            ////Use TestInitialize to run code before running each test 
            //[TestInitialize()]
            //public void MyTestInitialize()
            //{        
            //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //}

            ////Use TestCleanup to run code after each test has run
            //[TestCleanup()]
            //public void MyTestCleanup()
            //{        
            //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //}

            #endregion

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
