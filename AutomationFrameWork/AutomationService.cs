﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFrameWork
{
    class AutomationService
    {

        public static BrowserWindow mParentWindow { get; set; }
        public static BrowserWindow ParentWindow
        {
            get
            {
                if (mParentWindow == null)
                {
                    mParentWindow = TopParentWindow();
                }
                return mParentWindow;
            }

        }
        public static BrowserWindow TopParentWindow()
        {
            BrowserWindow win = new BrowserWindow();
            win.SearchProperties[UITestControl.PropertyNames.ClassName] = BrowserWindow.CurrentBrowser.ToString();
            return win;

        }
        public enum PropertyType
        {
            Name,
            Id,
            ClassName,
            InnerText,
            TagInstance

        }
        public static void EnterText<T>(PropertyType type, String propertyValue, String text) where T : HtmlControl
        {
            HtmlControl genericControl = (T)Activator.CreateInstance(typeof(T), new object[] { ParentWindow });
            if (type == PropertyType.Name)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.Name] = propertyValue;
            }
            else if (type == PropertyType.Id)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.Id] = propertyValue;
            }

            else if (type == PropertyType.InnerText)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.InnerText] = propertyValue;
            }
            Keyboard.SendKeys(genericControl, text);


        }

        public static void Click<T>(PropertyType type, String propertyValue ) where T : HtmlControl
        {
            HtmlControl genericControl = (T)Activator.CreateInstance(typeof(T), new object[] { ParentWindow });
            if (type == PropertyType.Name)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.Name] = propertyValue;
            }
            else if (type == PropertyType.Id)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.Id] = propertyValue;
            }

            else if (type == PropertyType.InnerText)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.InnerText] = propertyValue;
            }
            Mouse.Click(genericControl);

        }

        public static void WaitForControl<T>(PropertyType type, String propertyValue) where T : HtmlControl
        {
            HtmlControl genericControl = (T)Activator.CreateInstance(typeof(T), new object[] { ParentWindow });
            if (type == PropertyType.Name)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.Name] = propertyValue;
            }
            else if (type == PropertyType.Id)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.Id] = propertyValue;
            }

            else if (type == PropertyType.InnerText)
            {
                genericControl.SearchProperties[HtmlControl.PropertyNames.InnerText] = propertyValue;
            }
            genericControl.WaitForControlCondition(x => genericControl.WaitForControlPropertyEqual(type.ToString(), propertyValue));
            // genericControl.WaitForControlExist();

        }

        public static HtmlControl Getcontrol<T>(PropertyType type, String propertyValue) where T : HtmlControl
        {
            HtmlControl genericControl = (T)Activator.CreateInstance(typeof(T), new object[] { ParentWindow });
           
            return genericControl;

        }


        public static void SelectDDLItem<T>(PropertyType type, String propertyValue, String value) where T : HtmlComboBox
        {
            HtmlComboBox genericControl = (T)Activator.CreateInstance(typeof(T), new object[] { ParentWindow });


            if (type == PropertyType.Name)
            {
                genericControl.SearchProperties[HtmlComboBox.PropertyNames.Name] = propertyValue;
            }
            else if (type == PropertyType.Id)
            {
                genericControl.SearchProperties[HtmlComboBox.PropertyNames.Id] = propertyValue;
            }
            //else if (type == PropertyType.InnerText)
            //{
            //    genericControl.SearchProperties[HtmlComboBox.PropertyNames.InnerText] = propertyValue;
            //}
            //else if (type == PropertyType.ClassName)
            //{
            //    genericControl.SearchProperties[HtmlComboBox.PropertyNames.ClassName] = propertyValue;
            //}

            HtmlListItem uiSelectOption = new HtmlListItem(genericControl);
            if (value.ToLower() == "daily")
            {
                uiSelectOption.SearchProperties.Add(HtmlListItem.PropertyNames.DisplayText, "Daily");
            }
            if (value.ToLower() == "weekly")
            {
                uiSelectOption.SearchProperties.Add(HtmlListItem.PropertyNames.DisplayText, "Weekly");
            }
            if (value.ToLower() == "monthly")
            {
                uiSelectOption.SearchProperties.Add(HtmlListItem.PropertyNames.DisplayText, "Monthly");
            }
            uiSelectOption.Select();

        }

        public static void SelectCheckbox<T>(PropertyType type, String propertyValue, String value) where T : HtmlCheckBox
        {
            HtmlCheckBox genericControl = (T)Activator.CreateInstance(typeof(T), new object[] { ParentWindow });

            if (type == PropertyType.Name)
            {
                genericControl.SearchProperties[HtmlCheckBox.PropertyNames.Name] = propertyValue;
            }
            else if (type == PropertyType.Id)
            {
                genericControl.SearchProperties[HtmlCheckBox.PropertyNames.Id] = propertyValue;
            }
            else if (type == PropertyType.InnerText)
            {
                genericControl.SearchProperties[HtmlCheckBox.PropertyNames.InnerText] = propertyValue;
            }

            if (value.ToLower() == "check")
            {
                genericControl.Checked = true;
            }
            else
            {
                genericControl.Checked = false;

            }


        }





    }
}
